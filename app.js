const express = require('express');
const path = require('path');
const ejs = require('ejs');
const mysql = require('mysql');

const koneksi = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'testing',
});
koneksi.connect();

const app = express();

app.set('views',path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.urlencoded());

app.get('/', (req, res) => {
  koneksi.query('SELECT * FROM mahasiswa', (error, result) => {
    if (error) {
      console.log(error);
    } else {
      res.render('form', {
        data: result,
      });
    }
  });
});

app.post('/', (req, res) => {
  koneksi.query(`INSERT INTO mahasiswa (kode, nama) VALUES ('${req.body.kode}', '${req.body.nama}')`, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      koneksi.query('SELECT * FROM mahasiswa', (error, result) => {
        if (error) {
          console.log(error);
        } else {
          res.render('form', {
            data: result,
          });
        }
      });
    }
  });
});

app.listen(3003, () => {
  console.log('running on port 3003')
});